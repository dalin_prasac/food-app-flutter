import 'package:flutter/material.dart';
import 'package:food_app/routes.dart';
import 'package:food_app/screen/splash/splash.dart';
import 'package:provider/provider.dart';

import 'models/NavItem.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => NavItems(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        routes: Routes.routes,
        theme: ThemeData(
            fontFamily: 'Poppins',
            scaffoldBackgroundColor: Colors.white,
            appBarTheme: AppBarTheme(color: Colors.white, elevation: 0),
            visualDensity: VisualDensity.adaptivePlatformDensity),
        home: Splash(),
      ),
    );
  }
}
