import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:food_app/models/RecipeBundel.dart';
import 'package:food_app/utils/size_config.dart';

class RecipeBundleCard extends StatelessWidget {
  final RecipeBundle recipeBundle;
  final Function press;

  const RecipeBundleCard({Key key, this.recipeBundle, this.press})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final double defaulSize = SizeConfig.defaultSize;
    return GestureDetector(
      onTap: press,
      child: Container(
        decoration: BoxDecoration(
            color: recipeBundle.color,
            borderRadius: BorderRadius.circular(defaulSize * 1.5)), //15
        child: Row(
          children: <Widget>[
            Expanded(
                child: Padding(
              padding: EdgeInsets.all(defaulSize * 2), //20
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Spacer(),
                  Text(
                    recipeBundle.title,
                    style: TextStyle(
                        fontSize: defaulSize * 2.2, //22
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(height: defaulSize * 0.5), //5
                  Text(
                    recipeBundles[0].description,
                    style: TextStyle(color: Colors.white54),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Spacer(),
                  buildInfoRow(
                      iconSrc: "assets/icons/chef.svg",
                      text: "${recipeBundle.chefs} Recipe"),
                  SizedBox(height: defaulSize * 0.5), //5
                  buildInfoRow(
                      iconSrc: "assets/icons/chef.svg",
                      text: "${recipeBundle.chefs} Chefs"),
                  Spacer()
                ],
              ),
            )),
            SizedBox(
              width: defaulSize * 0.5, //5
            ),
            AspectRatio(
              aspectRatio: 0.71,
              child: Image.asset(
                recipeBundle.imageSrc,
                fit: BoxFit.cover,
                alignment: Alignment.centerLeft,
              ),
            )
          ],
        ),
      ),
    );
  }

  Row buildInfoRow({String iconSrc, text}) {
    return Row(
      children: <Widget>[
        SvgPicture.asset(iconSrc),
        SizedBox(width: SizeConfig.defaultSize * 0.5), //5
        Text(
          text,
          style: TextStyle(color: Colors.white),
        )
      ],
    );
  }
}
