import 'package:flutter/material.dart';
import 'package:food_app/models/RecipeBundel.dart';
import 'package:food_app/screen/home/components/recipe_bundle_card.dart';
import 'package:food_app/utils/size_config.dart';
import 'categorys.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: <Widget>[
          Category(),
          Expanded(
              child: Padding(
            padding:
                EdgeInsets.symmetric(horizontal: SizeConfig.defaultSize * 2),
            child: GridView.builder(
              itemCount: recipeBundles.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount:
                    SizeConfig.orientation == Orientation.landscape ? 2 : 1,
                mainAxisSpacing: 20,
                crossAxisSpacing:
                    SizeConfig.orientation == Orientation.landscape
                        ? SizeConfig.defaultSize * 2
                        : 0,
                childAspectRatio: 1.65,
              ),
              itemBuilder: (context, index) => RecipeBundleCard(
                recipeBundle: recipeBundles[index],
                press: () {
                  print("Card $index");
                },
              ),
            ),
          ))
        ],
      ),
    );
  }
}
