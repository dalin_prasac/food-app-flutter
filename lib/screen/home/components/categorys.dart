import 'package:flutter/material.dart';
import 'package:food_app/utils/constants.dart';
import 'package:food_app/utils/size_config.dart';

class Category extends StatefulWidget {
  @override
  _CategoryState createState() => _CategoryState();
}

class _CategoryState extends State<Category> {
  double defaulSize = SizeConfig.defaultSize;
  List<String> categorys = ["All", "Cambodia", " Thailand", "Korean", "Japan"];
  int selectIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: SizeConfig.defaultSize * 2), //20
      child: SizedBox(
        height: defaulSize * 3.5, //35
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: categorys.length,
            itemBuilder: (context, index) => buildCategoryItem(index)),
      ),
    );
  }

  Widget buildCategoryItem(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectIndex = index;
          print("Category $index");
        });
      },
      child: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(left: defaulSize * 2), //20
        padding: EdgeInsets.symmetric(
          horizontal: defaulSize * 2, //20
          vertical: defaulSize * 0.5, //5
        ),
        decoration: BoxDecoration(
            color:
                selectIndex == index ? Color(0xFFEFF3EE) : Colors.transparent,
            borderRadius: BorderRadius.circular(
              defaulSize * 1.6, //16
            )),
        child: Text(
          categorys[index],
          style: TextStyle(
              fontWeight: FontWeight.bold,
              color: selectIndex == index ? kPrimaryColor : Color(0xFFC2C2B5)),
        ),
      ),
    );
  }
}
